import { v4 as uuidv4 } from 'uuid';
import { normalize, NormUser } from './lab3/lab3.1.js';
import { validateUser } from './lab3/lab3.2.mjs';
import { filterByProps } from './lab3/lab3.3_6.mjs';
import { sortBy } from './lab3/lab3.4.mjs';
import { findBy } from './lab3/lab3.5.mjs';

/** ******** Your code here! *********** */

main();

async function main() {
  const originUrl = 'https://randomuser.me/api/?';
  let url = 'https://randomuser.me/api/?';

  async function fetchN(quantity = 50) {
    const response = await fetch(`${url}&results=${quantity}`);
    const json = await response.json();
    return json.results;
  }
  let allTeachers = [];
  let validated = [];
  let favorites = [];
  let filteredFav = [];
  let filteredTable = [];
  let tableTeachers = [];
  let render = '';
  const resetSortButton = document.getElementById('resetSort');

  document.getElementById('queryForm')
    .addEventListener('submit', async function (e) {
      e.preventDefault();
      const formData = new FormData(this);
      let query = '';
      for (const pair of formData.entries()) {
        console.log(pair[0], pair[1]);
        if (pair[1] == 'any') {

        } else if (query.includes('gender')) {
          query += `,${pair[1]}`;
        } else {
          query += `&${pair[0]}=${pair[1]}`;
        }
      }
      url = originUrl + query;
      console.log(url);
      console.log(originUrl);
      await normalizeAfterFetch();
      renderTop(validated);
      renderFavorite(favorites);
      renderTable(tableTeachers, 1, true);
    });

  console.log(await fetchN());

  async function normalizeAfterFetch(quantity = 50, concat = false) {
    if (concat) {
      allTeachers = normalize(await fetchN(quantity), [], allTeachers);
    } else {
      allTeachers = normalize(await fetchN(quantity), []);
    }
    //  const allTeachers = await fetchN()
    validated = [];
    favorites = [];
    filteredFav = [];
    filteredTable = [];
    tableTeachers = [];
    render = '';
    allTeachers.forEach((usr) => {
      if (validateUser(usr)) {
        validated.push(usr);
        if (usr.favorite) {
          favorites.push(usr);
        }
      }
    });
    filteredFav = [...favorites];
    filteredTable = [...validated];
    tableTeachers = [...validated];
  }

  await normalizeAfterFetch();

  const top = document.querySelector('div.top-teachers');

  function renderTop(arr) {
    arr.forEach((usr) => {
      const [first, last] = usr.full_name.split(' ');
      const havePicture = usr.picture_large !== null;
      render += `<figure>
      <div id="${usr.id}" class="${havePicture ? 'relative' : 'no-pic'} query">
      ${havePicture ? `<img src="${usr.picture_large}" alt="Alternative text">` : `<div>${first.charAt(0)}.${last.charAt(0)}</div>`}
          <svg  ${!usr.favorite ? 'style="display: none;"' : ''} class="star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
                            <path d="m135.78 50.46c0-2.01-1.52-3.259-4.564-3.748l-40.897-5.947-18.331-37.07c-1.031-2.227-2.363-3.34-3.992-3.34-1.629 0-2.96 1.113-3.992 3.34l-18.332 37.07-40.899 5.947c-3.041.489-4.562 1.738-4.562 3.748 0 1.141.679 2.445 2.037 3.911l29.656 28.841-7.01 40.736c-.109.761-.163 1.305-.163 1.63 0 1.141.285 2.104.855 2.893.57.788 1.425 1.181 2.566 1.181.978 0 2.064-.324 3.259-.977l36.58-19.229 36.583 19.229c1.142.652 2.228.977 3.258.977 1.089 0 1.916-.392 2.486-1.181.569-.788.854-1.752.854-2.893 0-.706-.027-1.249-.082-1.63l-7.01-40.736 29.574-28.841c1.414-1.412 2.119-2.716 2.119-3.911"
                                  transform="matrix(.12375 0 0 .12375 2.59 2.958)" fill="#fdbc4b"/>
                        </svg>
      </div>
      <figcaption>
        <div>${first}</div>
        <div>${last}</div>
      </figcaption>
      <small style="display: block; text-align: center; margin-top: 5px;">${usr.country}</small>
    </figure>`;
    });
    top.innerHTML = render;
    render = '';
    document.querySelectorAll('figure div.query')
      .forEach((node) => {
        node.addEventListener('click', (e) => {
          popupListener(node);
        });
      });
  }

  renderTop(validated);

  const right_arrow = document.querySelector('.right-arrow');
  const left_arrow = document.querySelector('.left-arrow');

  if ((favorites.length <= 5 && window.innerWidth >= 1041) || (favorites.length <= 3 && window.innerWidth < 1041)) {
    right_arrow.style.display = 'none';
    left_arrow.style.display = 'none';
  }

  const favoritesSection = document.querySelector('div.top-teachers.lower');

  function renderFavorite(arr) {
    arr.slice(0, 5)
      .forEach((usr) => {
        const [first, last] = usr.full_name.split(' ');
        const havePicture = usr.picture_large != null;
        render += `<figure class="visibility">
                  <div id="${usr.id}" class="${havePicture ? 'relative' : 'no-pic'} query">
      ${havePicture ? `<img src="${usr.picture_large}" alt="Alternative text">` : `<div>${first.charAt(0)}.${last.charAt(0)}</div>`}
                         </div>
                    <figcaption>
                        <div>${first}</div>
                        <div>${last}</div>
                    </figcaption>
                    <small style="display: block; text-align: center; margin-top: 5px;">${usr.country}</small>
                </figure>`;
      });
    favoritesSection.innerHTML = render;
    render = '';
    document.querySelectorAll('figure div.query')
      .forEach((node) => {
        node.addEventListener('click', (e) => {
          popupListener(node);
        });
      });
  }

  renderFavorite(favorites);

  const pop1 = document.getElementById('popup1');

  left_arrow.addEventListener('click', () => {
    const removedUsr = favorites.pop();
    favorites.unshift(removedUsr);
    const usr = favorites[0];
    const [first, last] = usr.full_name.split(' ');
    favoritesSection.lastElementChild.remove();
    const havePicture = usr.picture_large != null;
    render = `<figure class="visibility">
                    <div id="${usr.id}" class="${havePicture ? 'relative' : 'no-pic'} query">
      ${havePicture ? `<img src="${usr.picture_large}" alt="Alternative text">` : `<div>${first.charAt(0)}.${last.charAt(0)}</div>`}
                         </div>
                    <figcaption>
                        <div>${first}</div>
                        <div>${last}</div>
                    </figcaption>
                    <small style="display: block; text-align: center; margin-top: 5px;">${usr.country}</small>
                </figure>`;
    favoritesSection.insertAdjacentHTML('afterbegin', render);
    const current = favoritesSection.firstElementChild.firstElementChild;
    current.addEventListener('click', () => {
      popupListener(current);
    });
    render = '';
  });

  right_arrow.addEventListener('click', () => {
    const removedUsr = favorites.shift();
    favorites.push(removedUsr);
    console.log(favorites);
    let usr;
    if ((favorites.length == 4 && window.innerWidth <= 1041)) {
      usr = favorites[3];
    } else {
      usr = favorites[4];
    }
    const [first, last] = usr.full_name.split(' ');
    favoritesSection.firstElementChild.remove();
    const havePicture = usr.picture_large != null;
    render = `<figure class="visibility">
                     <div id="${usr.id}" class="${havePicture ? 'relative' : 'no-pic'} query">
      ${havePicture ? `<img src="${usr.picture_large}" alt="Alternative text">` : `<div>${first.charAt(0)}.${last.charAt(0)}</div>`}
                         </div>
                    <figcaption>
                        <div>${first}</div>
                        <div>${last}</div>
                    </figcaption>
                    <small style="display: block; text-align: center; margin-top: 5px;">${usr.country}</small>
                </figure>`;
    favoritesSection.insertAdjacentHTML('beforeend', render);
    const current = favoritesSection.lastElementChild.firstElementChild;
    current.addEventListener('click', () => {
      popupListener(current);
    });
    render = '';
  });

  const close = document.querySelectorAll('a.close');
  close.forEach((el) => {
    el.addEventListener('click', () => {
      el.parentElement.parentElement.style = '';
    });
  });

  const p1ul = document.getElementById('p1ul');

  function popupListener(node) {
    pop1.style.opacity = 1;
    pop1.style.display = 'unset';
    const id = node.getAttributeNode('id').value;
    const usr = validated.find((usr) => usr.id == id);
    p1ul.setAttribute('id', id);
    p1ul.innerHTML = `<div class="pack1 relative">
                <li>
                    <img class="avatar" src="${usr.picture_large ? usr.picture_large : 'https://hope.be/wp-content/uploads/2015/05/no-user-image.gif'}"
                         alt="">
                </li>
                <li>
                    <div>
                        <h2>${usr.full_name}</h2>
                             <img id="popupStar" class="small-star" ${!usr.favorite ? 'style="display: none;"' : ''}
                             src="https://previews.123rf.com/images/photoart23d/photoart23d1903/photoart23d190301182/119069240-star-symbol-icon-golden-simple-hollow-5-pointed-rounded-isolated-vector-illustration.jpg"
                             alt="">
                    </div>
                </li>
                <div class="pack2">
                    <li>${usr.city}, ${usr.country}</li>
                    <li>${usr.age}, ${usr.gender.charAt(0)}</li>
                    <li class="blue">${usr.email}</li>
                    <li>${usr.phone}</li>                 
                    <li><button class="favorite">${usr.favorite ? 'Remove from' : 'Add to'} favorite</button></li>
                </div>
            </div>
            <div>
                <li>${usr.note}</li>
                <li class="blue decor">toggle map</li>
            </div>`;
    const button = document.querySelector('button.favorite');
    const favPopStar = document.getElementById('popupStar');
    const svg = document.getElementById(usr.id).lastElementChild;
    button.addEventListener('click', () => {
      if (!usr.favorite) {
        usr.favorite = true;
        favorites.push(usr);
        button.innerHTML = 'Remove from favorite';
        favPopStar.style.display = 'unset';
        svg.style.display = 'unset';
        if ((favorites.length < 6 && window.innerWidth >= 1041) || (favorites.length < 4 && window.innerWidth < 1041)) {
          const [first, last] = usr.full_name.split(' ');
          const havePicture = usr.picture_large != null;
          render = `<figure class="visibility">
                    <div id="${usr.id}" class="${havePicture ? 'relative' : 'no-pic'} query">
      ${havePicture ? `<img src="${usr.picture_large}" alt="Alternative text">` : `<div>${first.charAt(0)}.${last.charAt(0)}</div>`}
                         </div>
                    <figcaption>
                        <div>${first}</div>
                        <div>${last}</div>
                    </figcaption>
                    <small style="display: block; text-align: center; margin-top: 5px;">${usr.country}</small>
                </figure>`;
          favoritesSection.insertAdjacentHTML('beforeend', render);
          const current = favoritesSection.lastElementChild.firstElementChild;
          current.addEventListener('click', () => {
            popupListener(current);
          });
          render = '';
        }
        if ((favorites.length == 6 && window.innerWidth >= 1041) || (favorites.length == 4 && window.innerWidth < 1041)) {
          right_arrow.style.display = 'unset';
          left_arrow.style.display = 'unset';
        }
      } else {
        usr.favorite = false;
        const indexDeleted = favorites.indexOf(usr);
        favorites.splice(indexDeleted, 1);
        button.innerHTML = 'Add to favorite';
        favPopStar.style.display = 'none';
        svg.style.display = 'none';
        const deleted = document.querySelector('div.top-teachers.lower').childNodes;
        console.log(deleted);
        deleted[indexDeleted].remove();
        if ((indexDeleted < 5 && window.innerWidth >= 1041) || (indexDeleted < 3 && window.innerWidth < 1041)) {
          if (favorites.length > deleted.length) {
            let newUsr;
            if ((indexDeleted < 5 && window.innerWidth >= 1041)) {
              newUsr = favorites[4] ? favorites[4] : favorites[favorites.length - 1];
            }
            if ((indexDeleted < 3 && window.innerWidth < 1041)) {
              newUsr = favorites[2] ? favorites[2] : favorites[favorites.length - 1];
            }
            if ((deleted.length < 3 && window.innerWidth < 1041) || (window.innerWidth >= 1041)) {
              const [first, last] = newUsr.full_name.split(' ');
              const havePicture = newUsr.picture_large != null;
              render = `<figure class="visibility">
                    <div id="${newUsr.id}" class="${havePicture ? 'relative' : 'no-pic'} query">
      ${havePicture ? `<img src="${newUsr.picture_large}" alt="Alternative text">` : `<div>${first.charAt(0)}.${last.charAt(0)}</div>`}
                         </div>
                    <figcaption>
                        <div>${first}</div>
                        <div>${last}</div>
                    </figcaption>
                    <small style="display: block; text-align: center; margin-top: 5px;">${newUsr.country}</small>
                </figure>`;
              favoritesSection.insertAdjacentHTML('beforeend', render);
              const current = favoritesSection.lastElementChild.firstElementChild;
              current.addEventListener('click', () => {
                popupListener(current);
              });
            }
          }
        }
        if ((favorites.length <= 5 && window.innerWidth >= 1041) || (favorites.length <= 3 && window.innerWidth < 1041)) {
          right_arrow.style.display = 'none';
          left_arrow.style.display = 'none';
        }
      }
      console.log(favorites);
    });
  }

  const filterForm = document.getElementById('filterForm');

  filterForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const formData = new FormData(filterForm);
    const props = {};

    // filterByProps(filtered, props)
    if (formData.get('country') != '') {
      props.country = formData.get('country');
    }
    if (formData.get('favorite') == 'on') {
      props.favorite = true;
    }
    if (formData.get('picture_large') == 'on') {
      props.picture_large = {
        val: null,
        cond: '!=',
      };
    }
    if (formData.get('age') != '') {
      props.age = {
        val: formData.get('age'),
        cond: formData.get('cond'),
      };
    }
    if (formData.get('gender') != null) {
      props.gender = formData.get('gender');
    }
    // for(let pair of formData.entries()) {
    //   props[pair[0]] = pair[1] == 'on' ? true : pair[1]
    // } // bar
    console.log(props);
    console.log(filterByProps(validated, props));

    //  let filterRes = filterByProps(filtered,props)
    // filteredFav = filterByProps(favorites, props);
    tableTeachers = filterByProps(validated, props);
    filteredTable = [...tableTeachers];
    resetSortButton.dispatchEvent(new Event('click'));
    renderTop(filterByProps(validated, props));
    //   renderFavorite(filteredFav);
    // renderTable(tableTeachers, 1);
  });

  const tableBody = document.getElementById('tableBody');
  const tablePages = document.querySelector('ul.stat-pages');

  function renderTable(arr, page = 1, withNext = false) {
    render = '';
    arr.slice(10 * (page - 1), 10 * page)
      .forEach((usr) => {
        const [first, last] = usr.full_name.split(' ');
        render += `<tr>
                    <td>${`${first} ${last}`}</td>
                    <td>${usr.age}</td>
                    <td>${usr.gender}</td>
                    <td>${usr.country}</td>
                </tr>`;
      });
    tableBody.innerHTML = render;
    render = '';
    let pages = Math.floor(arr.length / 10) + (arr.length % 10 ? 1 : 0);
    for (let i = 1; i <= pages; i++) {
      render += `<li ${i == page ? 'class="blue"' : ''}>${i}</li>`;
    }
    if (withNext) {
      render += '<li>Next</li>';
    }
    tablePages.innerHTML = render;
    render = '';
    tablePages.childNodes.forEach((node) => {
      const txt = node.innerText;
      if (txt != 'Next') {
        node.addEventListener('click', () => {
          if (txt == pages) renderTable(tableTeachers, txt, true);
          else renderTable(tableTeachers, txt);
        });
      } else {
        node.addEventListener('click', async () => {
          await normalizeAfterFetch(10, true);
          document.getElementById('filterForm')
            .dispatchEvent(new Event('reset'));
          resetSortButton.dispatchEvent(new Event('click'));
          renderTop(validated);
          renderFavorite(favorites);
          tableTeachers = [...validated];
          pages = Math.floor(tableTeachers.length / 10) + (tableTeachers.length % 10 ? 1 : 0);
          if (page == pages) renderTable(tableTeachers, pages, true);
          else renderTable(tableTeachers, (pages - 1));
        });
      }
    });
  }

  let sortProps = [];
  document.getElementById('sort')
    .childNodes
    .forEach((node) => {
      node.addEventListener('click', (e) => {
        e.stopPropagation();
        const prop = e.currentTarget.getAttribute('key');
        let index;

        function checkIfExists(arr, prop) {
          let res = false;
          arr.map((item) => {
            if (item.prop == prop) {
              res = true;
            }
            index = arr.indexOf(item);
          });
          return res;
        }

        if (!checkIfExists(sortProps, prop)) {
          sortProps.push({
            prop,
            direction: -1,
          });
        } else {
          sortProps[index].direction *= -1;
        }
        //     sortProps
        console.log(tableTeachers);
        sortBy(tableTeachers, sortProps);
        renderTable(tableTeachers);
        e.currentTarget.lastChild.style.display = 'unset';
        if (e.currentTarget.lastChild.innerText === 'arrow_drop_down') {
          e.currentTarget.lastChild.innerText = 'arrow_drop_up';
        } else {
          e.currentTarget.lastChild.innerText = 'arrow_drop_down';
        }
      });
    });

  resetSortButton
    .addEventListener('click', () => {
      sortProps = [];
      tableTeachers = [...filteredTable];
      document.querySelectorAll('.sortArrow')
        .forEach((node) => {
          node.style.display = 'none';
          node.innerText = 'arrow_drop_up';
        });
      renderTable(tableTeachers, 1, true);
    });

  const searchForm = document.getElementById('searchForm');

  searchForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const formData = new FormData(searchForm);
    console.log(formData.get('full_name'), formData.get('note'), formData.get('age'));
    const fields = {};
    if (formData.get('full_name') != '') fields.full_name = formData.get('full_name');
    if (formData.get('note') != '') fields.note = formData.get('note');
    if (formData.get('age') != '') fields.age = formData.get('age');
    const res = findBy(validated, fields.full_name, fields.note, fields.age);
    document.getElementById('filterForm')
      .dispatchEvent(new Event('reset'));
    resetSortButton.dispatchEvent(new Event('click'));
    renderTop(res);
    tableTeachers = [...res];
    renderTable(tableTeachers);
  });

  const pop2 = document.getElementById('popup2');

  document.querySelectorAll('button.addTeacherButton')
    .forEach((node) => {
      node.addEventListener('click', () => {
        pop2.style.opacity = '1';
        pop2.style.display = 'unset';
      });
    });

  document.getElementById('addTeacherForm')
    .addEventListener('submit', (e) => {
      e.preventDefault();
      const form = document.getElementById('addTeacherForm');
      const formData = new FormData(form);
      const miniUser = {};
      for (const pair of formData.entries()) {
        console.log(pair[0], pair[1]);
        miniUser[pair[0]] = pair[1];
      }
      console.log(miniUser);
      console.log({ ...Object.assign(new NormUser(), miniUser) });
      const temp = { ...Object.assign(new NormUser(), miniUser) };
      if (validateUser(temp)) {
        validated.push(temp);
        tableTeachers.push(temp);
        if (temp.favorite) {
          favorites.push(temp);
          if (favorites.length <= 5) {
            renderFavorite(favorites);
          }
        }
        document.getElementById('resetFilter')
          .dispatchEvent(new Event('reset'));
        resetSortButton
          .dispatchEvent(new Event('click'));
        form.dispatchEvent(new Event('reset'));
        renderTop(validated);
        tableTeachers = [...validated];
        renderTable(tableTeachers, 1, true);
        document.getElementById('p2close')
          .dispatchEvent(new Event('click'));
        fetch('http://localhost:3000/teachers', {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: JSON.stringify(temp), // body data type must match "Content-Type" header
        });
      } else {
        alert('Davai po novoi Misha, vse ...');
      }
      // dest.push(Object.assign({}, Object.assign(new NormUser(), usr)));
    });

  renderTable(validated, 1);

  console.log(window.innerWidth);
  console.log(validated);
  console.log(favorites);
  console.log(allTeachers);
}
