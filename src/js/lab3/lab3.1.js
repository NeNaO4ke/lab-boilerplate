//import { randomUserMock, additionalUsers } from './mock_for_L3.mjs';
import { v4 as uuidv4 } from 'uuid';

// console.log(normalize(randomUserMock, additionalUsers));
// const set = new Set();
// randomUserMock.forEach((item) => set.add(item.email));
// additionalUsers.forEach((item) => set.add(item.email));
// console.log(set.size);
// console.log(randomUserMock.length, additionalUsers.length);
// console.log(normalize(randomUserMock, additionalUsers).length);

function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

//console.log(randomDate(new Date(2012, 0, 1), new Date()));

export function NormUser(gender = null, title = null, firstname = null, lastname = null,
  city = null, state = null, country = null, postcode = null,
  latitude = null, longitude = null,
  offset = null, offsetDescription = null, email = null, bod = null,
  age = null, phone = null,
  pictureLarge = null, pictureThumbnail = null, idName = null,
  idNum = null, favorite = false, course = null, bg_color = null, note = null) {
  if(gender !== null){
    this.gender = gender.charAt(0).toUpperCase() + gender.slice(1);;
  }else
  this.gender = null;
  this.title = title;
  if (firstname == '' || firstname == null || lastname == '' || lastname == null) {
    this.full_name = 'unknown';
  } else {
    this.full_name = firstname + ' ' + lastname;
  }
  this.city = city;
  this.state = state;
  if (country === null) {
    this.country = Math.random()
      .toString(36)
      .substring(2, 15)
      .charAt(0)
      .toUpperCase() + Math.random()
      .toString(36)
      .substring(2, 15) + Math.random()
      .toString(36)
      .substring(2, 15);
  } else {
    this.country = country;
  }
  this.postcode = postcode;
  this.coordinates = {
    latitude,
    longitude,
  };
  this.timezone = {
    offset,
    description: offsetDescription,
  };
  this.email = email;
  if (bod === null) {
    this.b_date = randomDate(new Date(2000, 0, 1), new Date())
      .toISOString();
  } else {
    this.b_date = bod;
  }
  if (age === null) {
    this.age = Math.round((Math.random() * 50));
  } else {
    this.age = age;
  }
  this.phone = phone;
  this.picture_large = pictureLarge;
  this.picture_thumbnail = pictureThumbnail;
  if (idName == null || idName == '' || idNum == null || idNum == '') {
    this.id = uuidv4();
  } else {
    this.id = idName + idNum;
  }
  if(favorite === false){
    this.favorite = Math.random() < 0.15;
  }

  this.course = course;
  this.bg_color = bg_color;
  if (note == null) {
    this.note = Math.random()
      .toString(36)
      .substring(2, 15)
      .charAt(0)
      .toUpperCase() + Math.random()
      .toString(36)
      .substring(2, 15) + Math.random()
      .toString(36)
      .substring(2, 15);
  } else {
    this.note = note;
  }
  return this;
}



export function normalize(arr1, arr2, dest = []) {
  const idSet = new Set();

  const renameProp = function (o, new_key, old_key) {
    Object.defineProperty(o, new_key, {
      enumerable: true,
      configurable: true,
      writable: true,
      value: o[old_key]
    });
    delete o[old_key];
  };

  function extracted(usr) {
    dest.push(Object.assign({},
      new NormUser(usr.gender, usr.name.title, usr.name.first, usr.name.last, usr.location.city, usr.location.state, usr.location.country,
        usr.location.postcode, usr.location.coordinates.latitude, usr.location.coordinates.longitude, usr.location.timezone.offset,
        usr.location.timezone.description, usr.email, usr.dob.date, usr.dob.age, usr.phone, usr.picture.large, usr.picture.thumbnail,
        usr.id.name, usr.id.value)));
  }

  arr1.forEach(usr => {
    if ((usr.email != null && usr.email !== '') || usr.email === undefined) {
      if (usr.email === undefined) {
        extracted(usr);
      } else {
        if (!idSet.has(usr.email)) {
          idSet.add(usr.email);
          extracted(usr);
        }
      }
    }
  });
  arr2.forEach(usr => {
    if ((usr.email != null && usr.email !== '') || usr.email === undefined) {
      if (usr.email === undefined) {
        if (usr.b_day !== undefined) {
          renameProp(usr, 'b_date', 'b_day');
        }
        dest.push(Object.assign({}, Object.assign(new NormUser(), usr)));
      } else {
        if (!idSet.has(usr.email)) {
          idSet.add(usr.email);
          if (usr.b_day !== undefined) {
            renameProp(usr, 'b_date', 'b_day');
          }
          dest.push(Object.assign({}, Object.assign(new NormUser(), usr)));
        }
      }
    }
  });
  return dest;
}
