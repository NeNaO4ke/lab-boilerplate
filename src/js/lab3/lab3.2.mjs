import { additionalUsers, randomUserMock } from './mock_for_L3.mjs';
import { normalize } from './lab3.1.js';

//const formatted = normalize(randomUserMock, additionalUsers);

function validateString(string) {
  if (typeof string !== 'string') {
    return false;
  }
  if (string.charAt(0)
    .toUpperCase() === string.charAt(0) && string.charAt(0)
    .toUpperCase() !== string.charAt(0)) {
    return false;
  }
  return true;
}

function regexPhoneNumber(str) {
  //  const regexPhoneNumber = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
  //  console.log(regexPhoneNumber('+38-066-123-4567'))
  //  console.log(regexPhoneNumber('+(38)-066-123-4567'))
  //  console.log(regexPhoneNumber('+(380)-66-123-4567'))
  //  console.log(regexPhoneNumber('+(380)661234567'))
  //  console.log(regexPhoneNumber('+380661234567'))
  if(str == null)
    return false
  const regexPhoneNumber = /^[+]*\(?([0-9]{2,3})\)?([ -]?)([0-9]{3})([ -]?)([0-9]{4})/;
  if (str.match(regexPhoneNumber)) {
    return true;
  }
  return false;

}

function validateEmail(email) {
  // console.log(validateEmail('rew@t.g'));
  // console.log(validateEmail('re@w@t.g'));
  // console.log(validateEmail('rew@tg'));
  // console.log(validateEmail('rew@tg'));
  // console.log(validateEmail('g@d.a'));
  // console.log(validateEmail('g@.a'));
  const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (email.match(re)) {
    return true;
  } else {
    return false;
  }
}

export function validateUser(usr) {
  if (validateString(usr.full_name)) {
    if (validateString(usr.gender)) {
      if (validateString(usr.note)) {
  //      if (validateString(usr.state)) {
          if (validateString(usr.city)) {
            if (validateString(usr.country)) {
              if (typeof parseInt(usr.age, 10) === 'number') {
                if (parseInt(usr.age, 10) >= 0) {
                  if (regexPhoneNumber(usr.phone)) {
                    if (validateEmail(usr.email)) {
                      return true;
                    }
                  }
                }
              }
            }
          }
 //       }
      }
    }
  };
  return false;
}

//console.log(validateUser(formatted[0]));
