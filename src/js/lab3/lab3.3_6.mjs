import { additionalUsers, randomUserMock } from './mock_for_L3.mjs';
import { normalize } from './lab3.1.js';



const filterProps1 = {
  country: 'United States',
  age: 29
};
const filterPropsCond = {
  country: {
    val: 'France',
    cond: '!=',
  },
  age: {
    val: 71,
    cond: '==',
  }

};

function condSwitch(a, cond, val){
  let res = false
  switch (cond) {
    case '==':
      if (a == val) {
        return true;
      }
      break

    case '!=':
      if (a != val) {
        return true;
      }
      break
    case '>=':
      if (a >= val) {
        return true;
      }
      break
    case '<=':
      if (a <= val) {
        return true;
      }
      break

    default:
      if (a == this.val) {
        return true;
      }
      break
  }
  return res;
}

export function filterByProps(arr, filterProps) {
  let res = arr;
  Object.entries(filterProps)
    .forEach(([key, value]) => {
      if (typeof value === 'object' && value !== null) {
        res = res.filter(usr => condSwitch(usr[key], filterProps[key].cond,filterProps[key].val));
      } else {
        res = res.filter(usr => usr[key] === value);
      }
    });
  return res;
}

export function percentOfFilter(arr, filterProps) {
  return (filterByProps(arr, filterProps).length / arr.length * 100 + '%');
}

