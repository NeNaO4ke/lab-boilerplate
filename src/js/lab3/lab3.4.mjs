import { additionalUsers, randomUserMock } from './mock_for_L3.mjs';
import { normalize } from './lab3.1.js';



let sortByProps = [ {
  prop: 'age',
  direction: 1
}, {
  prop: 'full_name',
  direction: 1
},{
  prop: 'b_date',
  direction: -1
}];

let sortByProps2 = [{
  prop: 'country',
  direction: 1
}];

export function sortBy(array, sortBy) {
  array.sort(function (a, b) {
    let i = 0,
      result = 0;
    while (i < sortBy.length && result === 0) {
      result = sortBy[i].direction * (a[sortBy[i].prop].toString() < b[sortBy[i].prop].toString() ? -1 : (a[sortBy[i].prop].toString() > b[sortBy[i].prop].toString() ? 1 : 0));
      i++;
    }
    return result;
  });
  return array;
}


