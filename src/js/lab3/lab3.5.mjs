import { additionalUsers, randomUserMock } from './mock_for_L3.mjs';
import { normalize } from './lab3.1.js';



export function findBy(arr, name, note, age) {
  let res = arr
    if (name !== null && name !== undefined && name != '') {
        res = res.filter(usr => usr.full_name.includes(name));
    }
    if (note !== null && note !== undefined && note != '') {
        res = res.filter(usr => usr.note.includes(note));
    }
    if (age !== null && age !== undefined && age != '') {
        res = res.filter(usr => usr.age == age);
    }
 return res
}

//console.log(formatted.find(usr => usr.full_name ==='Eugene Kuhn' && usr.age === 64));
