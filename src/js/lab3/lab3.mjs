// import { randomUserMock, additionalUsers } from './mock_for_L3.mjs';
//
// function randomDate(start, end) {
//   return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
// }
//
// //console.log(randomDate(new Date(2012, 0, 1), new Date()));
//
// function NormUser(gender = null, title = null, firstname = null, lastname = null,
//   city = null, state = null, country = null, postcode = null,
//   latitude = null, longitude = null,
//   offset = null, offsetDescription = null, email = null, bod = null,
//   age = null, phone = null,
//   pictureLarge = null, pictureThumbnail = null, idName = null,
//   idNum = null, favorite = false, course = null, bg_color = null, note = null) {
//
//   this.gender = gender;
//   this.title = title;
//   if (firstname == '' || firstname == null || lastname == '' || lastname == null) {
//     this.full_name = 'Unknown';
//   } else {
//     this.full_name = firstname + ' ' + lastname;
//   }
//   this.city = city;
//   this.state = state;
//   if (country === null) {
//     this.country = Math.random()
//       .toString(36)
//       .substring(2, 15)
//       .charAt(0)
//       .toUpperCase() + Math.random()
//       .toString(36)
//       .substring(2, 15) + Math.random()
//       .toString(36)
//       .substring(2, 15);
//   } else {
//     this.country = country;
//   }
//   this.postcode = postcode;
//   this.coordinates = {
//     latitude,
//     longitude,
//   };
//   this.timezone = {
//     offset,
//     description: offsetDescription,
//   };
//   this.email = email;
//   if (bod === null) {
//     this.b_date = randomDate(new Date(2000, 0, 1), new Date())
//       .toISOString();
//   } else {
//     this.b_date = bod;
//   }
//   if (age === null) {
//     this.age = Math.round((Math.random() * 50));
//   } else {
//     this.age = age;
//   }
//   this.phone = phone;
//   this.picture_large = pictureLarge;
//   this.picture_thumbnail = pictureThumbnail;
//   if (idName == null || idName == '' || idNum == null || idNum == '') {
//     this.id = null;
//   } else {
//     this.id = idName + idNum;
//   }
//   this.favorite = favorite;
//   this.course = course;
//   this.bg_color = bg_color;
//   if (note == null) {
//     this.note = Math.random()
//       .toString(36)
//       .substring(2, 15)
//       .charAt(0)
//       .toUpperCase() + Math.random()
//       .toString(36)
//       .substring(2, 15) + Math.random()
//       .toString(36)
//       .substring(2, 15);
//   } else {
//     this.note = note;
//   }
//
//   return this;
// }
//
// let normalized = [];
//
// const renameProp = function (o, new_key, old_key) {
//   Object.defineProperty(o, new_key, {
//     enumerable: true,
//     configurable: true,
//     writable: true,
//     value: o[old_key]
//   });
//   delete o[old_key];
// };
//
// export function normalize(arr1, arr2, dest = []) {
//   const idSet = new Set();
//
//   function extracted(usr) {
//     dest.push(Object.assign({},
//       new NormUser(usr.gender, usr.name.title, usr.name.first, usr.name.last, usr.location.city, usr.location.state, usr.location.country,
//         usr.location.postcode, usr.location.coordinates.latitude, usr.location.coordinates.longitude, usr.location.timezone.offset,
//         usr.location.timezone.description, usr.email, usr.dob.date, usr.dob.age, usr.phone, usr.picture.large, usr.picture.thumbnail,
//         usr.id.name, usr.id.value)));
//   }
//
//   arr1.forEach(usr => {
//     if ((usr.email != null && usr.email !== '') || usr.email === undefined) {
//       if (usr.email === undefined) {
//         extracted(usr);
//       } else {
//         if (!idSet.has(usr.email)) {
//           idSet.add(usr.email);
//           extracted(usr);
//         }
//       }
//     }
//   });
//   arr2.forEach(usr => {
//     if ((usr.email != null && usr.email !== '') || usr.email === undefined) {
//       if (usr.email === undefined) {
//         if (usr.b_day !== undefined) {
//           renameProp(usr, 'b_date', 'b_day');
//         }
//         dest.push(Object.assign({}, Object.assign(new NormUser(), usr)));
//       } else {
//         if (!idSet.has(usr.email)) {
//           idSet.add(usr.email);
//           if (usr.b_day !== undefined) {
//             renameProp(usr, 'b_date', 'b_day');
//           }
//           dest.push(Object.assign({}, Object.assign(new NormUser(), usr)));
//         }
//       }
//     }
//   });
//   return dest;
// }
//
// // console.log(normalize(randomUserMock, additionalUsers, normalized));
// // const set = new Set();
// // randomUserMock.forEach(item => set.add(item.email));
// // additionalUsers.forEach(item => set.add(item.email));
// // console.log(set.size);
// // console.log(randomUserMock.length, additionalUsers.length);
// // console.log(normalize(randomUserMock, additionalUsers).length);
//
// const formatted = normalize(randomUserMock, additionalUsers);
//
// function validateString(string) {
//   if (typeof string !== 'string') {
//     return false;
//   }
//   if (string.charAt(0)
//     .toUpperCase() === string.charAt(0) && string.charAt(0)
//     .toUpperCase() !== string.charAt(0)) {
//     return false;
//   }
//   return true;
// }
//
// function regexPhoneNumber(str) {
//   // const regexPhoneNumber = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
//   //  console.log(regexPhoneNumber('+38-066-123-4567'))
//   //  console.log(regexPhoneNumber('+(38)-066-123-4567'))
//   //  console.log(regexPhoneNumber('+(380)-66-123-4567'))
//   //  console.log(regexPhoneNumber('+(380)661234567'))
//   //  console.log(regexPhoneNumber('+380661234567'))
//   const regexPhoneNumber = /^\+\(?([0-9]{2,3})\)?([ -]?)([0-9]{2,3})\2([0-9]{3})\2([0-9]{4})/;
//   if (str.match(regexPhoneNumber)) {
//     return true;
//   }
//   return false;
//
// }
//
// function validateEmail(email) {
//   // console.log(validateEmail('rew@t.g'));
// // // console.log(validateEmail('re@w@t.g'));
// // // console.log(validateEmail('rew@tg'));
// // // console.log(validateEmail('rew@tg'));
// // // console.log(validateEmail('g@d.a'));
// // // console.log(validateEmail('g@.a'));
//   const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//   if (email.match(re)) {
//     return true;
//   } else {
//     return false;
//   }
// }
//
// function validateUser(usr) {
//   if (validateString(usr.full_name)) {
//     if (validateString(usr.gender)) {
//       if (validateString(usr.note)) {
//         if (validateString(usr.state)) {
//           if (validateString(usr.city)) {
//             if (validateString(usr.country)) {
//               if (typeof usr.age === 'number') {
//                 if (usr.age >= 0) {
//                   if (regexPhoneNumber(usr.phone)) {
//                     if (validateEmail(usr.email)) {
//                       return true;
//                     }
//                   }
//                 }
//               }
//             }
//           }
//         }
//       }
//     }
//   }
//   ;
//   return false;
// }
//
// // console.log(validateUser(formatted[0]));
//
// const filterProps1 = {
//   country: 'United States',
//   age: 29
// };
// const filterPropsCond = {
//   country: {
//     val: 'France',
//     condition: function (a) {
//       if (a != this.val) {
//         return true;
//       }
//       return false;
//     }
//   },
//   age: {
//     val: 71,
//     condition: function (a) {
//       if (a >= this.val) {
//         return true;
//       }
//       return false;
//     }
//   }
//
// };
//
// function filterByProps(arr, filterProps) {
//   let res = arr;
//   Object.entries(filterProps)
//     .forEach(([key, value]) => {
//       if (typeof value === 'object' && value !== null) {
//         res = res.filter(usr => filterProps[key].condition(usr[key]));
//       } else {
//         res = res.filter(usr => usr[key] === value);
//       }
//     });
//   return res;
// }
//
// function percentOfFilter(arr, filterProps) {
//   return (filterByProps(arr, filterProps).length / arr.length * 100 + '%');
// }
//
// console.log(filterByProps(formatted, filterPropsCond));
// console.log(percentOfFilter(formatted, filterPropsCond));
//
// /*console.log(filterSimple(formatted, 'Norway'))
// console.log('NEXT')
// console.log(filterSimple(formatted, 'Norway', 28))
// console.log('NEXT')
// console.log(filterSimple(formatted, 'Norway', 28, 'male'))
// console.log('NEXT')
// console.log(filterSimple(formatted, 'Norway', 28, 'female', true))
// console.log('NEXT')
// console.log(filterSimple(formatted, null , null, 'female'))
// console.log('NEXT')
// console.log(filterSimple(formatted, 'Spain' , 'fdsg',))*/
//
// let sortedArr = [...formatted];
//
// let sortByProps = [{
//   prop: 'full_name',
//   direction: 1
// }, {
//   prop: 'age',
//   direction: 1
// }, {
//   prop: 'b_date',
//   direction: -1
// }];
//
// let sortByProps2 = [{
//   prop: 'country',
//   direction: 1
// }];
//
// function sortBy(array, sortBy) {
//   array.sort(function (a, b) {
//     let i = 0,
//       result = 0;
//     while (i < sortBy.length && result === 0) {
//       result = sortBy[i].direction * (a[sortBy[i].prop].toString() < b[sortBy[i].prop].toString() ? -1 : (a[sortBy[i].prop].toString() > b[sortBy[i].prop].toString() ? 1 : 0));
//       i++;
//     }
//     return result;
//   });
//   return array;
// }
//
// //console.log(sortBy(sortedArr, sortByProps2));
//
// function findBy(arr, name, note, age) {
//   return arr.find(usr => {
//     let bool = false;
//     if (name !== null && name !== undefined) {
//       if (usr.full_name.includes(name)) {
//         bool = true;
//       } else {
//         return false;
//       }
//     }
//     if (note !== null && note !== undefined) {
//       if (usr.note.includes(note)) {
//         bool = true;
//       } else {
//         return false;
//       }
//     }
//     if (age !== null && age !== undefined) {
//       if (usr.age === age) {
//         bool = true;
//       } else {
//         return false;
//       }
//     }
//     return bool;
//   });
// }
//
// //console.log(findBy(formatted, 'آدرینا صدر', null, undefined ));
// //console.log(formatted.find(usr => usr.full_name ==='Eugene Kuhn' && usr.age === 64));
//
// const used = process.memoryUsage().heapUsed / 1024 / 1024;
// console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
//
